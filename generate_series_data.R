generate_series_data <- function(func, n, N, sigma) {
    #param func: function to generate the pattern of observations over time
    #param n: number of observations per sample
    #param N: number of samples
    #param sigma: std deviation of each observation
    set.seed(123)
    n1 <- n + 1
    res <- matrix(nrow = N, ncol = n1)
    x <- 0
    
    # first row is special
    for (j in 1:n1) {
        res[1, j] <- func(x) + rnorm(1, mean = 0, sd = sigma)
        x <- x + 1   
    }

    # then subsequent rows
    for(i in 2:N) {
        for (j in 1:n) {
            res[i, j] <- res[i-1, j+1]
        }
        res[i, n+1] <- func(x) + rnorm(1, mean= 0, sd= sigma)
        x <- x + 1
    }

    return(res)

}
