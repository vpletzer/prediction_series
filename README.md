# Prediction Series

## How to test
```
Rscript run_tests.R
```
will run the tests.

## How to generate the data
```
Rscript generate_easy_data.R
```
will generate the data and save to two csv files, "train_easy.csv" and "test_easy.csv"